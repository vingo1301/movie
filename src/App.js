import logo from './logo.svg';
import './App.css';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import HomePage from './Pages/HomePage/HomePage';
import LoginPage from './Pages/LoginPage/LoginPage';
import NotFoundPage from './Pages/NotFoundPage/NotFoundPage';
import DetailPage from './Pages/DetailPage/DetailPage';
import Layout from './Components/HOC/Layout';

function App() {
  return (
    <div>
       <BrowserRouter>
        <Routes>
          <Route path='/' element={<Layout><HomePage></HomePage></Layout>}></Route>
          <Route path='/login' element={<LoginPage></LoginPage>}></Route>
          <Route path='*' element={<NotFoundPage></NotFoundPage>}></Route>
          <Route path='/detail/:id' element={<Layout><DetailPage></DetailPage></Layout>}></Route>
        </Routes>
       </BrowserRouter>
    </div>
  );
}

export default App;

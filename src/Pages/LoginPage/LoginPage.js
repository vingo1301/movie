import React from 'react';
import { Button, Checkbox, Form, Input, message } from 'antd';
import { userService } from '../services/userService';
import { useNavigate } from 'react-router-dom';
import { useDispatch } from 'react-redux';
import { SET_USER_INFO } from '../../redux/constant/constant';
import { userLocalService } from '../services/lolcalStorageService';
import animation from "../../assets/130393-christmas-car-riding.json";
import Lottie from 'lottie-react'
import { setLoginAction, setLoginActionService } from '../../redux/actions/userAction';

export default function LoginPage() {
  
  let navigate = useNavigate();
  let dispatch = useDispatch();
    const onFinish = (values) => {
        console.log('Success:', values);
        userService.postDangNhap(values).then((res) => {
          dispatch(setLoginAction(res.data.content));
          console.log(res);
          message.success("dang nhap thanh cong");
          // luu vao localStorage
          userLocalService.set(res.data.content);
          setTimeout(() => {
            // window.location.href = "/";
            navigate("/")
          },1000)
          
        }).catch((err) => {
          console.log(err);
          message.error("da co loi xay ra");
        })
      };
  
    const onFinishReduxThunk = (values) => {
          
          let onSuccess = () => {
            setTimeout(() => {
              // window.location.href = "/";
              navigate("/")
            },1000)
          }
          dispatch(setLoginActionService(values,onSuccess));
    }
    const onFinishFailed = (errorInfo) => {
        console.log('Failed:', errorInfo);
        
      };
      return (
        <div className='w-screen h-screen flex justify-center items-center'>
        <div className='container p-5 flex'>
            <div className='h-full w-1/2'>
            <Lottie animationData={animation} loop={true} />
            </div>
            <div className='h-full w-1/2'><Form
          name="basic"
          labelCol={{
            span: 8,
          }}
          wrapperCol={{
            span: 24,
          }}
          initialValues={{
            remember: true,
          }}
          onFinish={onFinishReduxThunk}
          onFinishFailed={onFinishFailed}
          autoComplete="off"
        >
          <Form.Item
            label="Username"
            name="taiKhoan"
            rules={[
              {
                required: true,
                message: 'Please input your username!',
              },
            ]}
          >
            <Input />
          </Form.Item>
    
          <Form.Item
            label="Password"
            name="matKhau"
            rules={[
              {
                required: true,
                message: 'Please input your password!',
              },
            ]}
          >
            <Input.Password />
          </Form.Item>
    
          
    
          <Form.Item
            wrapperCol={{
              offset: 8,
              span: 24,
            }}
          >
            <Button className="primary" htmlType="submit">
              Submit
            </Button>
          </Form.Item>
        </Form></div>
        
        </div>
        </div>
      );
}

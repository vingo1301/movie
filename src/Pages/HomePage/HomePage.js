import React, { useEffect,useState } from 'react'
import { movieService } from '../services/movieService'
import MovieList from './MovieList/MovieList';
import MovieTabs from './MovieTabs/MovieTabs';

export default function HomePage() {
  const [movieArr, setMovieArr] = useState([]);
  useEffect(() => {
    movieService.getDanhSachPhim().then((res) => {
            setMovieArr(res.data.content)
          })
          .catch((err) => {
           console.log(err);
          });
  },[])
  return (
    <div>
      <div className='container mx-auto'>
        <MovieList movieArr={movieArr}>
        </MovieList>
        <MovieTabs></MovieTabs>
      </div>
    </div>
  )
}

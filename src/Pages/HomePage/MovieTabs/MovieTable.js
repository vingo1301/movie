import moment from 'moment/moment'
import React from 'react'

export default function MovieTable({movie}) {
  return (
    <div className='flex p-3'>
        <img  className='w-24 h-40 mr-5 rounded object-cover' src={movie.hinhAnh} alt="" />
        <div>
            {/* moment js */}
            <h5 className='font-medium mb-5'>{movie.tenPhim}</h5>
            <div className='grid grid-cols-3 gap-4'>
                {movie.lstLichChieuTheoPhim.slice(0,9).map((item) =>{
                    return <span className='bg-red-500 text-white rounded p-2'>{moment(item.ngayChieuGioChieu).format("DD/MM/YYYY - hh:mm A")}</span>
                })}
            </div>
        </div>
    </div>
  )
}

import { userLocalService } from "../../Pages/services/lolcalStorageService"
import { SET_USER_INFO } from "../constant/constant"

const initialState = {
    userInfor: userLocalService.get(),
}

export const userReducer = (state = initialState, { type, payload }) => {
  switch (type) {

  case SET_USER_INFO:
    return { ...state, userInfor:payload }

  default:
    return state
  }
}

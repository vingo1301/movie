import { message } from "antd"
import axios from "axios"
import { userLocalService } from "../../Pages/services/lolcalStorageService"
import { userService } from "../../Pages/services/userService"
import { SET_USER_INFO } from "../constant/constant"

export const setLoginAction = (value) => {
    return {
        type: SET_USER_INFO,
        payload:value
    }
}
export const setLoginActionService = (userForm, onSuccess) => {
    return (dispatch) => {
        userService.postDangNhap(userForm).then((res) => {
            message.success("Dang nhap thanh cong!")
            // luu vao localStorage
            userLocalService.set(res.data.content);
            console.log(res);
            
            dispatch({
                type: SET_USER_INFO,
                payload: res.data.content,
            })
            onSuccess();
          })
          .catch((err) => {
           console.log(err);
          });
    }
}

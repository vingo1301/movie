import React from 'react'
import { Desktop, Mobile, Tablet } from '../HOC/Responsive'
import FooterDesktop from './FooterDesktop'
import FooterMobile from './FooterMobile'

export default function Footer() {
  return (
    <div>
        <Desktop><FooterDesktop></FooterDesktop></Desktop>
        <Mobile><FooterMobile></FooterMobile></Mobile>
        <Tablet><FooterMobile></FooterMobile></Tablet>
    </div>
  )
}
